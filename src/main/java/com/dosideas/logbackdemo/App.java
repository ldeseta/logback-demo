package com.dosideas.logbackdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws InterruptedException {
        int i = 0;
        while (true) {
            logPrueba("leito", i++);
            logPrueba("zim", i++);
            logPrueba("gir", i++);
            Thread.sleep(500);
        }
    }

    private static void logPrueba(String usuario, int i) {
        MDC.put("user", usuario);
        logger.debug("Hello, world as DEBUG, {}", i);
        logger.info("Hello, world as INFO, {}", i);
        logger.error("Hello, world as ERROR, {}", i);
        MDC.remove("user");
    }
}
